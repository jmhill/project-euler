# Justin' Project Euler Repo

## Goals

In order:

1. Grow my basic knowledge of mathematics
2. Practice with Deno (and therefore Typescript)
3. Build my own library of helpful utilities in Deno
4. Prepare for interview-style coding problems, which can sometimes be a little
   mathy (or at least leetcode-y)
