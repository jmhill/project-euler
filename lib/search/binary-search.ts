// Iterative approach
// Note: numbers array must already be sorted
export const binarySearch = function binarySearch(
  target: number,
  numbers: number[],
) {
  let floorIndex = -1;
  let ceilingIndex = numbers.length;

  // If there isn't at least 1 index between floor and ceiling,
  // we've run out of guesses and the number must not be present
  while (floorIndex + 1 < ceilingIndex) {
    const distance = ceilingIndex - floorIndex;
    const halfDistance = Math.floor(distance / 2); // Round down to avoid half index
    const guessIndex = floorIndex + halfDistance;

    const guessValue = numbers[guessIndex];

    if (guessValue === target) {
      return true;
    }

    if (guessValue > target) {
      ceilingIndex = guessIndex;
    } else {
      floorIndex = guessIndex;
    }
  }

  return false;
};
