import { assertEquals } from "../../deps.ts";
import { binarySearch } from "./binary-search.ts";

Deno.test("binarySearch returns true if element exists in array", () => {
  const testArray = [
    1,
    4,
    6,
    20,
    25,
    26,
    37,
    42,
    55,
    100,
    120,
    121,
    122,
    149,
  ];
  assertEquals(binarySearch(6, testArray), true);
});

Deno.test("binarySearch returns false if element does not exist in array", () => {
  const testArray = [
    1,
    4,
    6,
    20,
    25,
    26,
    37,
    42,
    55,
    100,
    120,
    121,
    122,
    149,
  ];
  assertEquals(binarySearch(148, testArray), false);
});
