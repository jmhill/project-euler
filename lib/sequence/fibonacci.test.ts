import { assertEquals } from "../../deps.ts";
import { fibonacciGenerator, nthFibonacci } from "./fibonacci.ts";

Deno.test("fibonacciGenerator yields fibonacci numbers on demand", () => {
  const sequence = fibonacciGenerator();
  assertEquals(sequence.next().value, 0);
  assertEquals(sequence.next().value, 1);
  assertEquals(sequence.next().value, 1);
  assertEquals(sequence.next().value, 2);

  for (let i = 0; i < 10; i++) {
    sequence.next();
  }

  assertEquals(sequence.next().value, 377);
});

Deno.test("fibonacciGenerator resets to zero when truthy value passed to next()", () => {
  const sequence = fibonacciGenerator();
  assertEquals(sequence.next().value, 0);
  assertEquals(sequence.next().value, 1);
  assertEquals(sequence.next().value, 1);
  assertEquals(sequence.next().value, 2);
  assertEquals(sequence.next(true).value, 0);
});

Deno.test("nthFibonacci returns the nth fibonacci number in sequence startin with 0, 1", () => {
  assertEquals(nthFibonacci(0), 0);
  assertEquals(nthFibonacci(1), 1);
  assertEquals(nthFibonacci(2), 1);
  assertEquals(nthFibonacci(3), 2);
  assertEquals(nthFibonacci(14), 377);
});
