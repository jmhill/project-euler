export const fibonacciGenerator = function* fibonacciGenerator(): Generator<
  number
> {
  let current = 0;
  let next = 1;
  while (true) {
    const reset = yield current;
    [current, next] = [next, next + current];
    if (reset) {
      current = 0;
      next = 1;
    }
  }
};

export const nthFibonacci = (n: number) => {
  let i = 0;
  const sequence = fibonacciGenerator();
  while (i < n) {
    sequence.next();
    i++;
  }
  return sequence.next().value;
};
