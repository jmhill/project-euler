type MultipleChecker = (y: number) => boolean;

export const isMultipleOf = (x: number): MultipleChecker => {
  return (y: number) => y % x === 0;
};

export const isEven = (x: number): boolean => x % 2 === 0;

export const isOdd = (x: number): boolean => x % 2 !== 0;
