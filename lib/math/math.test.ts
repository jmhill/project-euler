import { assertEquals } from "../../deps.ts";
import { isEven, isMultipleOf, isOdd } from "./math.ts";

Deno.test("Math exports function 'isMultipleOf'", () => {
  assertEquals(typeof isMultipleOf, "function");
});

Deno.test("Exported function 'isMultipleOf' returns a function", () => {
  const result = isMultipleOf(1);
  assertEquals(typeof result, "function");
});

Deno.test("isMultipleOf(x)(y) returns false if y is not a multiple of x", () => {
  const isMultipleOf3 = isMultipleOf(3);
  const result = isMultipleOf3(7);
  assertEquals(result, false);
});

Deno.test("isMultipleOf(x)(y) returns true if y IS a multiple of x", () => {
  const isMultipleOf3 = isMultipleOf(3);
  const result = isMultipleOf3(6);
  assertEquals(result, true);
});

Deno.test("isMultipleOf(x)(y) returns true if y is 0", () => {
  const result = isMultipleOf(5)(0);
  assertEquals(result, true);
});

Deno.test("isEven returns true for even numbers", () => {
  const result = isEven(2);
  assertEquals(result, true);
});

Deno.test("isEven returns false for odd numbers", () => {
  const result = isEven(3);
  assertEquals(result, false);
});

Deno.test("isOdd returns true for odd numbers", () => {
  const result = isOdd(3);
  assertEquals(result, true);
});

Deno.test("isOdd returns false for even numbers", () => {
  const result = isOdd(4);
  assertEquals(result, false);
});
