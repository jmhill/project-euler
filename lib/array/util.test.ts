import { assertEquals } from "../../deps.ts";
import { rangeGenerator, range } from "./util.ts";

Deno.test("range(x, y) returns array with numbers from x to y, inclusive", () => {
  const myArray = range(1, 5);
  assertEquals(myArray[0], 1);
  assertEquals(myArray[1], 2);
  assertEquals(myArray[2], 3);
  assertEquals(myArray[3], 4);
  assertEquals(myArray[4], 5);
});

Deno.test("rangeGenerator(x, y) yields values from x to y, inclusive", () => {
  const myRange = rangeGenerator(1, 5);
  assertEquals(myRange.next().value, 1);
  assertEquals(myRange.next().value, 2);
  assertEquals(myRange.next().value, 3);
  assertEquals(myRange.next().value, 4);
  assertEquals(myRange.next().value, 5);
  const final = myRange.next();
  assertEquals(final.value, undefined);
  assertEquals(final.done, true);
});
