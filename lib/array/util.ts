export const rangeGenerator = function* range(start: number, end: number) {
  for (let i = start; i <= end; i++) {
    yield i;
  }
};

export const range = (
  start: number,
  end: number,
) => [...rangeGenerator(start, end)];

export const sum = (arr: number[]) =>
  arr.reduce((previous, current) => {
    return previous + current;
  }, 0);
