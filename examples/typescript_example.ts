const name = Deno.args[0];

const greeter = (name: string): string => `Hello, ${name}`;

console.log(greeter(name));
