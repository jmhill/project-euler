import { assertEquals } from "../deps.ts";
import { solution1 } from "./1.ts";

Deno.test("Problem 1: The sum of all multiples of 3 and 5 in range 1..999 is 233168", () => {
  assertEquals(solution1(), 233168);
});
