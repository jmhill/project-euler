// Problem 1:
// If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
// Find the sum of all the multiples of 3 or 5 below 1000.

import { isMultipleOf } from "../lib/math/math.ts";
import { rangeGenerator, sum } from "../lib/array/util.ts";

// Problem solution
// Note: it's important to realize we're interested in the single set of all numbers 1..999 that
// are EITHER multiples of 3 or 5 -
// if you try to just get all the multiples of 3 in 1..999 and all the multiples of 5 in 1..999,
// you'll end up with duplicates since some numbers (e.g. 15) are multiples of both 3 and 5

export const solution1 = () => {
  const multiples = [];

  for (const i of rangeGenerator(1, 999)) {
    const isMultiple = isMultipleOf(3)(i) || isMultipleOf(5)(i);
    if (isMultiple) {
      multiples.push(i);
    }
  }

  const solution = sum(multiples);

  return solution;
};
