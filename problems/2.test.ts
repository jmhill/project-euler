import { assertEquals } from "../deps.ts";
import { solution2 } from "./2.ts";

Deno.test("Problem 2: The sum of all even-valued Fibonnaci numbers less than 4 million is 4,613,732", () => {
  assertEquals(solution2(), 4613732);
});
